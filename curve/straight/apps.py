from django.apps import AppConfig


class StraightConfig(AppConfig):
    name = 'straight'
